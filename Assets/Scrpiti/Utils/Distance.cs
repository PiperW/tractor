﻿using UnityEngine;

public static class Distance
{
    private const double EARTH_RADIUS = 6378.137; // Radius of earth in KM
    private const float KM_TO_METERS = 1000f;
    private const int DEGREES = 180;

    /// <summary>
    /// Calculates distance between two sets of coordinates, taking into account the curvature of the earth.
    /// </summary>
    /// <param name="lat1">Start latitude</param>
    /// <param name="lon1">Start longitude</param>
    /// <param name="lat2">Current latitude</param>
    /// <param name="lon2">Current longitude</param>
    /// <returns>Dsitance between the coordinates</returns>
    public static double CalculateDistance(float lat1, float lon1, float lat2, float lon2)
    {
        double distance = 0;

        float latitudeDistance = lat2 * Mathf.PI / DEGREES - lat1 * Mathf.PI / DEGREES;
        float longitudeDistance = lon2 * Mathf.PI / DEGREES - lon1 * Mathf.PI / DEGREES;
        
        float haversine = Mathf.Sin(latitudeDistance / 2) * Mathf.Sin(latitudeDistance / 2) +
            Mathf.Cos(lat1 * Mathf.PI / DEGREES) * Mathf.Cos(lat2 * Mathf.PI / DEGREES) *
            Mathf.Sin(longitudeDistance / 2) * Mathf.Sin(longitudeDistance / 2);

        double angularDistance = 2 * Mathf.Atan2(Mathf.Sqrt(haversine), Mathf.Sqrt(1 - haversine));

        distance = EARTH_RADIUS * angularDistance;
        distance = distance * KM_TO_METERS; // convert the distance to meters

        return distance;
    }
}
