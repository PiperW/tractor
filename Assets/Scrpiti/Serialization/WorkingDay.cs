﻿using System;
using System.Collections.Generic;

[Serializable]
public class WorkingDay
{
    public int CurrentYear;
    public Dictionary<string, double> DayToDistanceTravelled;
}
