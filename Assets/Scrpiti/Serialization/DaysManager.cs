﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GPS))]
public class DaysManager : MonoBehaviour
{
    public static DaysManager INSTANCE = null;
    
    private const string FILE_NAME = "WorkingDay";
    private const string DATE_DISPLAY = "d";

    private WorkingDay workingDay;
    private GPS gpsTracker;
    private MainApp mainApp;

    private DateTime currentDate; // current day

    private void Awake()
    {
        if (INSTANCE == null)
        {
            INSTANCE = this;
        }
        else
        {
            if (INSTANCE != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);

        LoadDataOnStart();
    }

    private void Start()
    {
        gpsTracker = GetComponent<GPS>();
        mainApp = GetComponent<MainApp>();
        currentDate = DateTime.Now;
    }

    /// <summary>
    /// Save the data upon quitting the application
    /// </summary>
    private void OnApplicationQuit()
    {
        SaveData(mainApp.ZiuaCurenta);
    }

    /// <summary>
    /// Return the saved working day
    /// </summary>
    /// <returns>Saved working day</returns>
    public WorkingDay GetWorkingDay()
    {
        LoadData();
        return workingDay;
    }

    /// <summary>
    /// Delete de file with the saved data
    /// </summary>
    public void DeleteData()
    {
        if (EntityManager.DeleteData(FILE_NAME))
        {
            // file deleted
        }
        else
        {
            // file not deleted
        }
    }

    /// <summary>
    /// Save the current progress
    /// </summary>
    /// <param name="distance">Distance travelled until the stop button has been pressed</param>
    public void SaveData(double distance)
    {
        WorkingDay localWorkingDay = GetWorkingDay();
        string currentDay = currentDate.Date.ToString(DATE_DISPLAY);

        if (localWorkingDay != null && localWorkingDay.DayToDistanceTravelled != null)
        {
            if (!localWorkingDay.DayToDistanceTravelled.ContainsKey(currentDay))
            {
                localWorkingDay.DayToDistanceTravelled.Add(currentDay, distance);
            }
            else
            {
                localWorkingDay.DayToDistanceTravelled[currentDay] += distance;
            }
        }

        SaveData();
    }

    /// <summary>
    /// Get the distance travelled the current day
    /// </summary>
    /// <returns>The distance travelled the current day</returns>
    public double GetCurrentDayDistance()
    {
        WorkingDay localWorkingDay = GetWorkingDay();
        string currentDay = currentDate.Date.ToString(DATE_DISPLAY);

        if (localWorkingDay != null && localWorkingDay.DayToDistanceTravelled != null)
        {
            if (!localWorkingDay.DayToDistanceTravelled.ContainsKey(currentDay))
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }

        return localWorkingDay.DayToDistanceTravelled[currentDay];
    }

    /// <summary>
    /// Adds the distance in all the days
    /// </summary>
    /// <returns>The distance travelled added with each day</returns>
    public double GetTotalDistance()
    {
        WorkingDay localWorkingDay = GetWorkingDay();
        string currentDay = currentDate.Date.ToString(DATE_DISPLAY);
        double totalDistance = 0;

        if (localWorkingDay != null && localWorkingDay.DayToDistanceTravelled != null && localWorkingDay.DayToDistanceTravelled.Keys.Count > 0)
        {
            var days = localWorkingDay.DayToDistanceTravelled.Keys;

            foreach (var day in days)
            {
                if (!day.Equals(currentDay))
                {
                    totalDistance += localWorkingDay.DayToDistanceTravelled[day];
                }
            }
        }

        return totalDistance;
    }

    /// <summary>
    /// Save the data
    /// </summary>
    private void SaveData()
    {
        EntityManager.SaveData(workingDay, FILE_NAME);
    }

    /// <summary>
    /// Load the data
    /// </summary>
    private void LoadData()
    {
        workingDay = EntityManager.LoadData<WorkingDay>(FILE_NAME);
    }

    /// <summary>
    /// Load the data upon application start
    /// </summary>
    private void LoadDataOnStart()
    {
        LoadData();

        // if no data has been saved
        if (workingDay == null)
        {
            CreateEmptyWorkingDay();
        }
        else
        {
            // if the saved year is not the current one then delete the file since contains old info
            if (workingDay.CurrentYear != currentDate.Year)
            {
                if (EntityManager.DeleteData(FILE_NAME))
                {
                    // succesfully deleted
                    CreateEmptyWorkingDay();
                }
                else
                {
                    // error in deletion
                }
            }
        }
    }

    private void CreateEmptyWorkingDay()
    {
        workingDay = new WorkingDay
        {
            CurrentYear = currentDate.Year,
            DayToDistanceTravelled = new Dictionary<string, double>()
        };
        SaveData();
    }
}
