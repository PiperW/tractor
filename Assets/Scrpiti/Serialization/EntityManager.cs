﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class EntityManager
{
    /// <summary>
    /// Serialize the data and save it to a file
    /// </summary>
    /// <typeparam name="T">Type of the data to be saved</typeparam>
    /// <param name="dataToSave">Data type to be saved</param>
    /// <param name="dataFileName">Name of the file in which the data will be saved</param>
    public static void SaveData<T>(T dataToSave, string dataFileName)
    {
        string tempPath = Path.Combine(Application.persistentDataPath, "data");
        tempPath = Path.Combine(tempPath, dataFileName + ".txt");

        //Create Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(tempPath));
        }

        try
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            using (FileStream fileStream = File.Open(tempPath, FileMode.OpenOrCreate))
            {
                binaryFormatter.Serialize(fileStream, dataToSave);
            }
        }
        catch (Exception e)
        {
        }
    }

    /// <summary>
    /// Load the data from the file
    /// </summary>
    /// <typeparam name="T">Type of the data that will be deserialized from the file</typeparam>
    /// <param name="dataFileName">Name of the file from where the saved data can be found</param>
    /// <returns>Deserialized data</returns>
    public static T LoadData<T>(string dataFileName)
    {
        string tempPath = Path.Combine(Application.persistentDataPath, "data");
        tempPath = Path.Combine(tempPath, dataFileName + ".txt");

        //Exit if Directory or File does not exist
        if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
        {
            return default(T);
        }

        if (!File.Exists(tempPath))
        {
            return default(T);
        }

        try
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            using (FileStream fileStream = File.Open(tempPath, FileMode.Open))
            {
                return (T)binaryFormatter.Deserialize(fileStream);
            }
        }
        catch (Exception e)
        {
        }

        return default(T);
    }

    /// <summary>
    /// Delete the saved file
    /// </summary>
    /// <param name="dataFileName">File name</param>
    /// <returns>The deletion status</returns>
    public static bool DeleteData(string dataFileName)
    {
        bool success = false;

        //Load Data
        string tempPath = Path.Combine(Application.persistentDataPath, "data");
        tempPath = Path.Combine(tempPath, dataFileName + ".txt");

        //Exit if Directory or File does not exist
        if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
        {
            //Debug.LogWarning("Directory does not exist");
            return false;
        }

        if (!File.Exists(tempPath))
        {
            //Debug.Log("File does not exist");
            return false;
        }

        try
        {
            File.Delete(tempPath);
            //Debug.Log("Data deleted from: " + tempPath.Replace("/", "\\"));
            success = true;
        }
        catch (Exception e)
        {
            //Debug.LogWarning("Failed To Delete Data: " + e.Message);
        }

        return success;
    }
}
