﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GPS : MonoBehaviour
{
    private const int DISTANCE_UPDATE_RATE = 4; // the rate at which the cumulatedDistance will be calculated

    private float originalLatitude; // the initial latitude
    private float originalLongitude; // the initial longitude

    private float currentLatitude; // the current latitude
    private float currentLongitude; // the current longitude

    private bool setOriginalValues = true; // if true, the original position will be the current position

    public Text distanceText; // a text showing the distance travelled
    public Text gpsText; // a text showing the position + errors
    private string errorText; // the errors that will be shown on the screen

    [HideInInspector]
    public bool startTracking = false; // true if the START button has been pressed
    private bool isUpdating = false; // if true, start the position update

    private int distanceUpdateRate = DISTANCE_UPDATE_RATE; // rate counter
    public double CumulatedDistance { get; set; } // the total distance travelled
    private double distance; // the distance travelled between the initial position and the current one

    private float cumulatedLatitude = 0;
    private float cumulatedLongitude = 0;
    private float noOfCoordinatesRetrieved = 0;
    private float averageLatitude;
    private float averageLongitude;

    private void Start()
    {
        CumulatedDistance = DaysManager.INSTANCE.GetCurrentDayDistance();
    }

    private void Update()
    {
        if(startTracking && !isUpdating)
        {
            if (distanceUpdateRate == DISTANCE_UPDATE_RATE)
            {
                distance = 0;
            }
            StartCoroutine(GetLocation());
            isUpdating = !isUpdating;
        }
    }

    IEnumerator GetLocation()
    {
        if (!Input.location.isEnabledByUser)
        {
            yield return new WaitForSeconds(3);
        }

        Input.location.Start();

        int maxWait = 3;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            errorText += " Timed out |";
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            errorText += " Location failed |";
            yield break;
        } 
        else
        {

            //if original value has not yet been set save coordinates of player on app start
            if (setOriginalValues)
            {
                originalLatitude = (averageLatitude == 0 ? Input.location.lastData.latitude : averageLatitude);
                originalLongitude = (averageLongitude == 0 ? Input.location.lastData.longitude : averageLongitude);
                setOriginalValues = false;
            }

            //overwrite current lat and lon everytime
            currentLatitude = Input.location.lastData.latitude;
            currentLongitude = Input.location.lastData.longitude;

            cumulatedLatitude += currentLatitude;
            cumulatedLongitude += currentLongitude;
            noOfCoordinatesRetrieved++;

            averageLatitude = cumulatedLatitude / noOfCoordinatesRetrieved;
            averageLongitude = cumulatedLongitude / noOfCoordinatesRetrieved;

            gpsText.text = errorText + "\n";
            gpsText.text += "Old Location: " + originalLatitude + " | " + originalLongitude + "\n";
            gpsText.text += "New Location: " + averageLatitude + " | " + averageLongitude;

            //calculate the distance between where the player was when the app started and where they are now.
            distance = Distance.CalculateDistance(originalLatitude, originalLongitude, averageLatitude, averageLongitude);

            CumulateDistance(distance);

            distanceText.text = "Distance: " + CumulatedDistance;
        }

        isUpdating = !isUpdating;
        Input.location.Stop();
    }

    /// <summary>
    /// Add the distances at every DISTANCE_UPDATE_RATE 
    /// </summary>
    /// <param name="distance">The current distance compared to the starting point</param>
    private void CumulateDistance(double distance)
    {
        if (distanceUpdateRate == 0)
        {
            distanceUpdateRate = DISTANCE_UPDATE_RATE;
            CumulatedDistance = CumulatedDistance + distance;
            setOriginalValues = true;
            cumulatedLatitude = 0;
            cumulatedLongitude = 0;
            noOfCoordinatesRetrieved = 0;
        }
        else
        {
            distanceUpdateRate--;
        }
    }
}
