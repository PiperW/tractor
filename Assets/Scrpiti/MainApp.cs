﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GPS))]
public class MainApp : MonoBehaviour
{
    private const string START = "START";
    private const string STOP = "STOP";

    public Text buttonText;

    #region USER_INPUT
    public InputField nrRanduriInput;
    public InputField distantaRanduriInput;

    private int nrRanduri;
    private float distantaRanduri;
    #endregion

    #region DAYS_SPECIFIC_TEXT
    public InputField ziuaCurentaText;
    public InputField totalCampanieText;

    public double ZiuaCurenta { get; set; }
    #endregion

    private DateTime currentDate; // current day

    private GPS gpsTracker;
    private bool buttonStartPressed = false;
    private bool canCalculate = true; // if the current day is between the designated period then calculate the distance travelled
                                       // DEBUG: set it true

    private double totalDistance;

    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep; // Keep the device awake
        currentDate = DateTime.Now;

        // If it's between SEPTEMBER and DECEMBER
        if (currentDate.Month >= 9 && currentDate.Month <= 12)
        {
            canCalculate = true;
        }
        else
        {
            //canCalculate = false;
            // DEBUG: comment
        }

        gpsTracker = GetComponent<GPS>();
        totalDistance = DaysManager.INSTANCE.GetTotalDistance();
        totalCampanieText.text = totalDistance.ToString();
        ziuaCurentaText.text = DaysManager.INSTANCE.GetCurrentDayDistance().ToString();
    }

    private void Update()
    {
        double todayDistance = gpsTracker.CumulatedDistance; // DEBUG: DaysManager.INSTANCE.GetCurrentDayDistance();
        ZiuaCurenta = todayDistance * nrRanduri * distantaRanduri;
        ziuaCurentaText.text = ZiuaCurenta.ToString();
        totalCampanieText.text = (totalDistance + ZiuaCurenta) + "";
    }

    /// <summary>
    /// Set the number of... randuri
    /// </summary>
    public void SetNrRanduri()
    {
        try
        {
            nrRanduri = int.Parse(nrRanduriInput.text);
            print("Nr Randuri: " + nrRanduri);
        }
        catch (FormatException e)
        {
            // not good
        }
    }

    /// <summary>
    /// Set the distance between the... randuri
    /// </summary>
    public void SetDistantaRanduri()
    {
        try
        {
            distantaRanduri = float.Parse(distantaRanduriInput.text);
            print("Distanta Randuri: " + distantaRanduri);
        } 
        catch (FormatException e)
        {
            // not good
        }

    }

    /// <summary>
    /// Start or stop the distance record
    /// </summary>
    public void ToggleButton()
    {
        buttonStartPressed = !buttonStartPressed;
        if (buttonStartPressed)
        {
            buttonText.text = STOP;
        }
        else
        {
            buttonText.text = START;
            DaysManager.INSTANCE.SaveData(ZiuaCurenta);
        }

        if (canCalculate)
        {
            gpsTracker.startTracking = buttonStartPressed;
        }
    }
}
